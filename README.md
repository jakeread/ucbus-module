## D51 Module, Round Two

For mixed volume production of various network endpoints / routers, this board is the collected set of boilerplate utilities need to boot and use the ATSAMD51: 

- JTAG Programming Header (SWD Pins)
- Reset Switch
- 5v in -> local 3v3 reg
- Bypass Caps
- 32kHz Xtal
- 16MHz Xtal 
- LEDS: Power, Clock, Error
- USB Connector 
- RS485 PHY w/ DE, RE, and Logic-Selectable Termination Resistor 
- DIP Switch for local configuration, i.e. bus-drop-address 
- SAMD51 

Most of the other D51 pins are broken out on 0.05" spaced castellated pins. The module is about as tiny as I could flex it, 20.3 x 28.6mm. The notion is to make *lots* of these, and solder them on to more ad-hoc driver boards. 

**V02**

I updated this module to break out the 5th SERCOM, so I could ostensibly use *all* of them in a circuit. I also routed the debug lights that I had forgotten about (whoops) and swapped from a surface mount programming header to a thru-hole, not soldered on by default, but a 'lock' header pattern, hopefully it will just touch-and-program pretty well. 

![routed](log/2020-08-25_routed.png)
![schematic](log/2020-08-25_schematic.png)

70 of these are on the way. 

**V01**

![fab](log/2020-07-28_ucbus-module-fab.jpg)
![routed](log/2020-06-29_routed.png)
![schem](log/2020-06-29_schematic.png)

### Part Numbers

| Part | Identifier | Digikey PN |
| --- | --- | --- |
| RS485 Driver | --- | MAX13450EAUD+-ND |
| Screw Terminal | --- | 277-1860-ND |
| DIP Switch | --- | CT3111-ND |
| XTAL 32KHz | --- | XC1617CT-ND |
| XTAL 16MHz | --- | SER4046CT-ND |
| 0402 LED Red | D2 | 516-3060-1-ND |
| 0402 LED Green | D3, D4 | 516-3066-1-ND |
| JTAG Program Header | J2 *was err specd as 1175-1629-ND* | 609-3695-1-ND |
| Reset Switch | --- | CKN10685CT-ND |
| D51 | --- | ATSAMD51J19A-MUTCT-ND |
| 3v3 600mA VREG | --- | AP2112K-3.3TRG1DIDKR-ND |
| USB Micro Plug | --- | 609-4613-1-ND |