## UCBus Module Log

## Revision 2 Code Notes

The only pin swap so far is DE for RE. 

## Revision 2 Circuit Notes

Still need to update module footprint for new / addnl pins, document JTAG location... donot short there. 

## Revision 3 (future) Notes

Recall your notes from the grbl-port-log.md in osap, about timing: it seems like you want a bus line ~ 25Mhz bit rate, consider building a PHY for some fancy SPI bus: want virtual channels, delineated with a wider word-per-byte... those can route to interrupt / thread lines, yeah. 

Maybe the module doesn't have a PHY? Maybe the next chip isn't a D51?? 

## Part Numbers

| Part | Identifier | Digikey PN |
| --- | --- | --- |
| RS485 Driver | --- | MAX13450EAUD+-ND |
| Screw Terminal | --- | 277-1860-ND |
| DIP Switch | --- | CT3111-ND |
| XTAL 32KHz | --- | XC1617CT-ND |
| XTAL 16MHz | --- | SER4046CT-ND |
| 0805 10uH | --- | 445-17073-1-ND |
| 0402 LED Red | D1 | 516-3060-1-ND |
| 0402 LED Green | D2, D3 | 516-3066-1-ND |
| JTAG Program Header | J2 *was err specd as 1175-1629-ND* | 609-3695-1-ND |
| Reset Switch | --- | CKN10685CT-ND |
| D51 | --- | ATSAMD51J19A-MUTCT-ND |
| 3v3 600mA VREG | --- | AP2112K-3.3TRG1DIDKR-ND |
| USB Micro Plug | --- | 609-4613-1-ND |

## 2020 08 25 

Did manage to update this, am satisfied. Broke out the last SERCOM, routed lights, changed programming header. Am satisfied, 70 are out to fab. 

## 2020 08 20 

I *do* need to revision this... OK, have broken out the 6th SERCOM, so I have SER1 on the RS485 module, then SER0, SER2, SER3, SER4 and SER5 broken out on pads... so I could do a 4-port module with 2 spare for IC control, or a 6-port 3D Lattice Module, so long as one of them can live with the RS485 module... and the old pinout hasn't changed, save for the DE -> RE swap. 

Routed lights.. now I think I am done with this. I'll do the stepper, then come back to this when I assemble BOMs for both to have them mfg'd. 

## 2020 08 10 Notes from Fab V01

Ambiguous silk on Y2: MHz XTAL: see datasheet, see silk, silk mark is for Pin 4, not one. 

Since fab, update to 0805 footprint in passives / changed some spacing on the board. Most notably, at L1. Seeed needs part reference designators somewhere, this is how they check (manually) that placement will be correct. 

Also caught an error in the BOM, I spec'd the shrouded 2x5 program header: `| JTAG Program Header | J2 *was err specd as 1175-1629-ND* | 609-3695-1-ND |` that doesn't fit. The correct PN, shroudless, does fit. Whoops. 

If there's a big rev of these, or maybe even without changing old footprints, could squeeze DE off of PB16, and open PB16,17,22,23 to castellated pins: this will give you external access to SER5 in parallel to existing access to SER0, SER2, SER3, and SER4. As it stands, a board with four neighbours can only run one other digital connection / SERCOM application. There's maybe space to add the four castellated pins just beside the USB plug. 

Did spec Resistors for LEDs as 10k, now knocked down to 470R on schematic, pls update boms. 

Woof. Didn't route error light or clock light pins to the micro. Welp.

Actually not sure about this GND pin on the lower left 9-pin port. Is it NC? Airwire thinks it is. Silk looks good. Easy beep test. 

Might consider, depending on the results from that bughunt, adding some hella 5V Capacitance to avoid power dips when the USB line is active, or do whatever other circuit work you need to isolate those behaviours? Notes in the OSAP log at 2020-07-29. 

## 2020 06 27 

OK, found a big ol' error in the program header, nice I caught that before sending out. Hopefully none more?? 

Last rights is the silkscreen. 

## 2020 06 26 

OK, third day at this. When I shut it all down last night and went to sit in the park I realized that *the move* here might actually be to make a module mass-produced, and lean on our PNP to assemble the simple side. For the stepper, the additional kit would only be two A4950s, sense resistors, BFCs and Ceramic bypass, and some protection resistors (optional). Those could all be hand-solder size parts, likely. 

The biggest change in approach that makes me feel sane about this is to take the giant connector off of the module - it'll have the D51, 5vReg, Prog and USB Connectors, Crystals, Reset and RS485 driver, oh and DIP Switch. The goddamn dip switch. Then I can put varied size connectors on various boards, and satisfy the Stepper, BLDC, Heatbed, Router, Hotend all with this thing.

This'll be a real pinch though, to design. I think it mostly becomes a problem on the steppers because of the screw positions. 

An even bolder move would be to exclude the RS485 driver as well... maybe eventually you want CAN. Whatever, just break the CAN pins out. Doof. 

It's been historically difficult to squish a module on board with a stepper. I think this would have to be long and skinny. 

I wonder if *the move* on this one is to modify from the old module out. I think I'll go from the stepper. 

Moduuling it up... might be 'the move' to go for 0.050" spaced castellated pins? I don't have a tonne of space to break these things out. Will see. 

The DIP Switch is a lot, maybe I should go for the ultra micro. Nope, this is the smallest they come. 

In order to really proceed I need to update the JTAG footprint to reflect the header space and connector drop... 

Most of the way through this I think. Feels like a repeat of yesterday. Next up is the most challenging part, picking pins to break out, etc. The far right / bottom side of this thing might be a challenge to get to. I can't forget I need eight (8!) pins for the goddang DIP Switch still. To get real, I'm probably going to hit all of these D51 pins. Yowsa. Whether or not I'll be able to successfully escape all of them is a question. 

Continuing the come along, I like the pinouts, just the last run of routes left. Might even get to tuck things in a bit ++ on the left. 

OK, I've just finished routing this, it sneaks in around 20x28.5mm, and here I've it roughed out on a stepper 'daughter' board. 

![routed-01](2020-06-26_ucbus-module-routed-01.png)
![routed-01](2020-06-26_ucbus-module-routed-02.png)

Cleaned up,

![routed-03](2020-06-26_ucbus-module-routed-03.png)

Here's the final (I think?) schematic.

![schem](2020-06-26_schematic.png)

So, that's kind of 'it' for now. To proceed, I want do due dilligence, etc, probably best to do that tomorrow with fresh eyes. Blessed that this is basically the same schematic / parts library as I used for the old module, so fingers crossed I've not messed anything up. 

- silk
- check / double / triple check board design 
- will need to pre-panelize yourself 
- setup decent BOM export / footprint values... 