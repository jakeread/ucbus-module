<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="microcontrollers">
<packages>
<package name="QFN-64-9X9MM-SMALLPAD">
<description>&lt;h3&gt;64-pin QFN 9x9mm, 0.5mm pitch&lt;/h3&gt;
&lt;p&gt;Package used by ATmega128RFA1&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.atmel.com/Images/Atmel-8266-MCU_Wireless-ATmega128RFA1_Datasheet.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-4.492" y1="-4.5" x2="4.508" y2="-4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="-4.5" x2="4.508" y2="4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="4.5" x2="-4.492" y2="4.5" width="0.09" layer="51"/>
<wire x1="-4.492" y1="4.5" x2="-4.492" y2="-4.5" width="0.09" layer="51"/>
<wire x1="-4.6" y1="4.6" x2="-4.6" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="4.6" x2="-4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.6" y2="4.1" width="0.2032" layer="21"/>
<circle x="-4.842" y="4.85" radius="0.2" width="0" layer="21"/>
<circle x="-3.442" y="3.45" radius="0.2" width="0.09" layer="51"/>
<smd name="26" x="0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="25" x="0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="24" x="-0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="27" x="1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="28" x="1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="22" x="-1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="21" x="-1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="6" x="-4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="5" x="-4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="4" x="-4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="7" x="-4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="8" x="-4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="3" x="-4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="2" x="-4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="9" x="-4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="10" x="-4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="1" x="-4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="16" x="-4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="15" x="-4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="14" x="-4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="17" x="-3.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="18" x="-3.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="13" x="-4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="12" x="-4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="19" x="-2.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="20" x="-2.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="11" x="-4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="29" x="2.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="30" x="2.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="31" x="3.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="32" x="3.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="33" x="4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="34" x="4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="35" x="4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="36" x="4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="37" x="4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="38" x="4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="39" x="4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="40" x="4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="41" x="4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="42" x="4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="43" x="4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="44" x="4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="45" x="4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="46" x="4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="48" x="4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="49" x="3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="50" x="3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="51" x="2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="52" x="2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="53" x="1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="54" x="1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="55" x="0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="56" x="0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="57" x="-0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="58" x="-0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="59" x="-1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="60" x="-1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="61" x="-2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="62" x="-2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="63" x="-3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="64" x="-3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="4.6" y1="-4.6" x2="4.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-4.6" x2="4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.1" y2="-4.6" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="0" dx="4.8" dy="4.8" layer="1" cream="no"/>
<polygon width="0.127" layer="31">
<vertex x="1.03" y="1.03"/>
<vertex x="1.03" y="2.17"/>
<vertex x="2.17" y="2.17"/>
<vertex x="2.17" y="1.03"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.17" y="1.03"/>
<vertex x="-2.17" y="2.17"/>
<vertex x="-1.03" y="2.17"/>
<vertex x="-1.03" y="1.03"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.17" y="-2.17"/>
<vertex x="-2.17" y="-1.03"/>
<vertex x="-1.03" y="-1.03"/>
<vertex x="-1.03" y="-2.17"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.03" y="-2.17"/>
<vertex x="1.03" y="-1.03"/>
<vertex x="2.17" y="-1.03"/>
<vertex x="2.17" y="-2.17"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.57" y="-0.57"/>
<vertex x="-0.57" y="0.57"/>
<vertex x="0.57" y="0.57"/>
<vertex x="0.57" y="-0.57"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="ATSAMD51J">
<pin name="GND" x="-35.56" y="-132.08" length="middle"/>
<pin name="VDDCORE" x="-35.56" y="-45.72" length="middle"/>
<pin name="VDDANA" x="-35.56" y="-15.24" length="middle"/>
<pin name="VDDIO" x="-35.56" y="0" length="middle"/>
<pin name="PA00/XIN32/SER1-0/TC2-0" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="PA01/XOUT32/SER1-1/TC2-1" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="PA02/ADC0-1/DAC-0" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="PA03/ANAREF-VREFA/ADC0-1" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="PA07/ADC0-7/SER0-3/TC1-1" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" x="43.18" y="-27.94" length="middle" rot="R180"/>
<pin name="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" x="43.18" y="-30.48" length="middle" rot="R180"/>
<pin name="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" x="43.18" y="-33.02" length="middle" rot="R180"/>
<pin name="PA14/XIN0/SER2-2/SER4-2/TC3-0" x="43.18" y="-35.56" length="middle" rot="R180"/>
<pin name="PA15/XOUT0/SER2-3/SER4-3/TC3-1" x="43.18" y="-38.1" length="middle" rot="R180"/>
<pin name="PA16/SER1-0/SER3-1/TC2-0/TCC0-4" x="43.18" y="-40.64" length="middle" rot="R180"/>
<pin name="PA17/SER1-1/SER3-0/TC2-1/TCC0-5" x="43.18" y="-43.18" length="middle" rot="R180"/>
<pin name="PA18/SER1-2/SER3-2/TC3-0" x="43.18" y="-45.72" length="middle" rot="R180"/>
<pin name="PA19/SER1-3/SER3-3/TC3-1" x="43.18" y="-48.26" length="middle" rot="R180"/>
<pin name="PA20/SER5-2/SER3-2/TC7-0" x="43.18" y="-50.8" length="middle" rot="R180"/>
<pin name="PA21/SER5-3/SER3-3/TC7-1" x="43.18" y="-53.34" length="middle" rot="R180"/>
<pin name="PA22/SER3-0/SER5-1/TC4-0" x="43.18" y="-55.88" length="middle" rot="R180"/>
<pin name="PA23/SER3-1/SER5-0/TC4-1" x="43.18" y="-58.42" length="middle" rot="R180"/>
<pin name="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" x="43.18" y="-60.96" length="middle" rot="R180"/>
<pin name="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" x="43.18" y="-63.5" length="middle" rot="R180"/>
<pin name="PA27/GCLK-1" x="43.18" y="-66.04" length="middle" rot="R180"/>
<pin name="PA30/SER7-2/SER1-2/TC6-0/SWCLK" x="43.18" y="-68.58" length="middle" rot="R180"/>
<pin name="PA31/SER7-3/SER1-3/TC6-1/SWDIO" x="43.18" y="-71.12" length="middle" rot="R180"/>
<pin name="PB00/ADC0-12/SER5-2/TC7-0" x="43.18" y="-78.74" length="middle" rot="R180"/>
<pin name="PB01/ADC0-13/SER5-3/TC7-1" x="43.18" y="-81.28" length="middle" rot="R180"/>
<pin name="PB03/ADC0/SER5-1/TC6" x="43.18" y="-86.36" length="middle" rot="R180"/>
<pin name="PB04/ADC1-6" x="43.18" y="-88.9" length="middle" rot="R180"/>
<pin name="PB05/ADC1-7" x="43.18" y="-91.44" length="middle" rot="R180"/>
<pin name="PB06/ADC1-8" x="43.18" y="-93.98" length="middle" rot="R180"/>
<pin name="PB07/ADC1-9" x="43.18" y="-96.52" length="middle" rot="R180"/>
<pin name="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" x="43.18" y="-99.06" length="middle" rot="R180"/>
<pin name="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" x="43.18" y="-101.6" length="middle" rot="R180"/>
<pin name="PB10/SER4-2/TC5-0/TCC0-4" x="43.18" y="-104.14" length="middle" rot="R180"/>
<pin name="PB11/SER4-3/TC5-1/TCC0-5" x="43.18" y="-106.68" length="middle" rot="R180"/>
<pin name="PB12/SER4-0/TC4-0" x="43.18" y="-109.22" length="middle" rot="R180"/>
<pin name="PB13/SER4-1/TC4-1" x="43.18" y="-111.76" length="middle" rot="R180"/>
<pin name="PB14/SER4-2/TC5-0" x="43.18" y="-114.3" length="middle" rot="R180"/>
<pin name="PB15/SER4-3/TC5-1" x="43.18" y="-116.84" length="middle" rot="R180"/>
<pin name="PB16/SER5-0/TC6-0" x="43.18" y="-119.38" length="middle" rot="R180"/>
<pin name="PB17/SER5-1/TC6-1" x="43.18" y="-121.92" length="middle" rot="R180"/>
<pin name="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" x="43.18" y="-124.46" length="middle" rot="R180"/>
<pin name="PB23/XOUT1/SER1-3/SER5-3/TC7-1" x="43.18" y="-127" length="middle" rot="R180"/>
<pin name="PB30/SER7-0/SER5-1/TC0-0/SWO" x="43.18" y="-129.54" length="middle" rot="R180"/>
<pin name="RESETN" x="-35.56" y="-119.38" length="middle"/>
<pin name="PB31/SER7-1/SER5-0/TC0-1" x="43.18" y="-132.08" length="middle" rot="R180"/>
<pin name="PB02/ADC0-14/SER5-0/TC6-0" x="43.18" y="-83.82" length="middle" rot="R180"/>
<wire x1="-30.48" y1="5.08" x2="38.1" y2="5.08" width="0.254" layer="94"/>
<wire x1="38.1" y1="5.08" x2="38.1" y2="-137.16" width="0.254" layer="94"/>
<wire x1="38.1" y1="-137.16" x2="-30.48" y2="-137.16" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-137.16" x2="-30.48" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-142.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VSW" x="-35.56" y="-30.48" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD51J" prefix="U">
<gates>
<gate name="G$1" symbol="ATSAMD51J" x="0" y="0"/>
</gates>
<devices>
<device name="QFN64" package="QFN-64-9X9MM-SMALLPAD">
<connects>
<connect gate="G$1" pin="GND" pad="7 22 33 47 54 P$1"/>
<connect gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0" pad="1"/>
<connect gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1" pad="2"/>
<connect gate="G$1" pin="PA02/ADC0-1/DAC-0" pad="3"/>
<connect gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1" pad="4"/>
<connect gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" pad="13"/>
<connect gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" pad="14"/>
<connect gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" pad="15"/>
<connect gate="G$1" pin="PA07/ADC0-7/SER0-3/TC1-1" pad="16"/>
<connect gate="G$1" pin="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" pad="17"/>
<connect gate="G$1" pin="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" pad="18"/>
<connect gate="G$1" pin="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" pad="19"/>
<connect gate="G$1" pin="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" pad="20"/>
<connect gate="G$1" pin="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" pad="29"/>
<connect gate="G$1" pin="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" pad="30"/>
<connect gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0" pad="31"/>
<connect gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1" pad="32"/>
<connect gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0/TCC0-4" pad="35"/>
<connect gate="G$1" pin="PA17/SER1-1/SER3-0/TC2-1/TCC0-5" pad="36"/>
<connect gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0" pad="37"/>
<connect gate="G$1" pin="PA19/SER1-3/SER3-3/TC3-1" pad="38"/>
<connect gate="G$1" pin="PA20/SER5-2/SER3-2/TC7-0" pad="41"/>
<connect gate="G$1" pin="PA21/SER5-3/SER3-3/TC7-1" pad="42"/>
<connect gate="G$1" pin="PA22/SER3-0/SER5-1/TC4-0" pad="43"/>
<connect gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1" pad="44"/>
<connect gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" pad="45"/>
<connect gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" pad="46"/>
<connect gate="G$1" pin="PA27/GCLK-1" pad="51"/>
<connect gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK" pad="57"/>
<connect gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO" pad="58"/>
<connect gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0" pad="61"/>
<connect gate="G$1" pin="PB01/ADC0-13/SER5-3/TC7-1" pad="62"/>
<connect gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0" pad="63"/>
<connect gate="G$1" pin="PB03/ADC0/SER5-1/TC6" pad="64"/>
<connect gate="G$1" pin="PB04/ADC1-6" pad="5"/>
<connect gate="G$1" pin="PB05/ADC1-7" pad="6"/>
<connect gate="G$1" pin="PB06/ADC1-8" pad="9"/>
<connect gate="G$1" pin="PB07/ADC1-9" pad="10"/>
<connect gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" pad="11"/>
<connect gate="G$1" pin="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" pad="12"/>
<connect gate="G$1" pin="PB10/SER4-2/TC5-0/TCC0-4" pad="23"/>
<connect gate="G$1" pin="PB11/SER4-3/TC5-1/TCC0-5" pad="24"/>
<connect gate="G$1" pin="PB12/SER4-0/TC4-0" pad="25"/>
<connect gate="G$1" pin="PB13/SER4-1/TC4-1" pad="26"/>
<connect gate="G$1" pin="PB14/SER4-2/TC5-0" pad="27"/>
<connect gate="G$1" pin="PB15/SER4-3/TC5-1" pad="28"/>
<connect gate="G$1" pin="PB16/SER5-0/TC6-0" pad="39"/>
<connect gate="G$1" pin="PB17/SER5-1/TC6-1" pad="40"/>
<connect gate="G$1" pin="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" pad="49"/>
<connect gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1" pad="50"/>
<connect gate="G$1" pin="PB30/SER7-0/SER5-1/TC0-0/SWO" pad="59"/>
<connect gate="G$1" pin="PB31/SER7-1/SER5-0/TC0-1" pad="60"/>
<connect gate="G$1" pin="RESETN" pad="52"/>
<connect gate="G$1" pin="VDDANA" pad="8"/>
<connect gate="G$1" pin="VDDCORE" pad="53"/>
<connect gate="G$1" pin="VDDIO" pad="21 34 48 56"/>
<connect gate="G$1" pin="VSW" pad="55"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="DX4R005HJ5_100">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="DX4R005HJ5">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="21"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="21"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="D-" x="-0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="ID" x="0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="GND" x="1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="25" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="DX4R005HJ5_64">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="USB_MICRO_609-4613-1-ND">
<smd name="HD0" x="-3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD4" x="-3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="HD5" x="3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="D+" x="0" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<text x="4.9275" y="1.2125" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-4.3925" y="1.13" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="HD1" x="-1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD2" x="1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD3" x="3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<wire x1="-4.7" y1="-1.45" x2="4.7" y2="-1.45" width="0.127" layer="51"/>
<text x="0" y="-1.3" size="0.8128" layer="51" font="vector" align="bottom-center">\\ PCB Edge /</text>
<wire x1="-3.9" y1="3" x2="-3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-3.9" y1="-2.5" x2="3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.9" y1="-2.5" x2="3.9" y2="3" width="0.127" layer="51"/>
<wire x1="3.9" y1="3" x2="-3.9" y2="3" width="0.127" layer="51"/>
<wire x1="-3.9" y1="1.1" x2="-3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.9" y1="1.1" x2="3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="3" x2="1.7" y2="3" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3" x2="-1.8" y2="3" width="0.127" layer="21"/>
<wire x1="4.4" y1="3" x2="4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-4.4" y1="3" x2="-4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-3.9" y1="3.6" x2="-3.9" y2="3.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="3.6" x2="3.9" y2="3.8" width="0.127" layer="21"/>
</package>
<package name="CASTELLATED_PIN_0.1">
<pad name="P$1" x="0" y="0" drill="1.016" diameter="1.8796" stop="no"/>
<polygon width="0.127" layer="1">
<vertex x="0" y="0.889"/>
<vertex x="0.635" y="0.889" curve="-90"/>
<vertex x="0.839" y="0.635"/>
<vertex x="0.839" y="-0.635" curve="-90"/>
<vertex x="0.635" y="-0.889"/>
<vertex x="0" y="-0.889"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="0" y="0.889"/>
<vertex x="0.635" y="0.889" curve="-90"/>
<vertex x="0.839" y="0.635"/>
<vertex x="0.839" y="-0.635" curve="-90"/>
<vertex x="0.635" y="-0.889"/>
<vertex x="0" y="-0.889"/>
</polygon>
<polygon width="0.254" layer="29">
<vertex x="0" y="0.889"/>
<vertex x="0.635" y="0.889" curve="-90"/>
<vertex x="0.889" y="0.635"/>
<vertex x="0.889" y="-0.635" curve="-90"/>
<vertex x="0.635" y="-0.889"/>
<vertex x="0" y="-0.889"/>
</polygon>
<polygon width="0.254" layer="30">
<vertex x="0" y="0.889"/>
<vertex x="0.635" y="0.889" curve="-90"/>
<vertex x="0.889" y="0.635"/>
<vertex x="0.889" y="-0.635" curve="-90"/>
<vertex x="0.635" y="-0.889"/>
<vertex x="0" y="-0.889"/>
</polygon>
</package>
<package name="CASTELLATED_PIN_0.05">
<pad name="P$1" x="0" y="0" drill="0.508" diameter="1.016" stop="no"/>
<polygon width="0.127" layer="1">
<vertex x="0" y="0.4445"/>
<vertex x="0.381" y="0.4445" curve="-90"/>
<vertex x="0.585" y="0.1905"/>
<vertex x="0.585" y="-0.254" curve="-90"/>
<vertex x="0.381" y="-0.4445"/>
<vertex x="0" y="-0.4445"/>
</polygon>
<polygon width="0.127" layer="16">
<vertex x="0" y="0.4445"/>
<vertex x="0.381" y="0.4445" curve="-90"/>
<vertex x="0.585" y="0.1905"/>
<vertex x="0.585" y="-0.254" curve="-90"/>
<vertex x="0.381" y="-0.4445"/>
<vertex x="0" y="-0.4445"/>
</polygon>
<polygon width="0.254" layer="29">
<vertex x="0" y="0.4445"/>
<vertex x="0.381" y="0.4445" curve="-90"/>
<vertex x="0.635" y="0.127"/>
<vertex x="0.635" y="-0.127" curve="-90"/>
<vertex x="0.381" y="-0.4445"/>
<vertex x="0" y="-0.4445"/>
</polygon>
<polygon width="0.254" layer="30">
<vertex x="0" y="0.4445"/>
<vertex x="0.381" y="0.4445" curve="-90"/>
<vertex x="0.635" y="0.127"/>
<vertex x="0.635" y="-0.127" curve="-90"/>
<vertex x="0.381" y="-0.4445"/>
<vertex x="0" y="-0.4445"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="USB-1">
<wire x1="6.35" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-1.27" size="2.54" layer="94">USB</text>
<text x="-4.445" y="-1.905" size="1.27" layer="95" font="vector" rot="R90">&gt;Name</text>
<text x="8.255" y="-1.905" size="1.27" layer="96" font="vector" rot="R90">&gt;Value</text>
<pin name="D+" x="5.08" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D-" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="VBUS" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="-2.54" y="5.08" visible="pad" length="short" rot="R270"/>
</symbol>
<symbol name="CONN_01_CASTELLATED">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.6096" layer="94"/>
<text x="-1.27" y="0" size="1.778" layer="95" font="vector" rot="R180" align="center-left">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="0" y1="1.27" x2="0" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="-0.762" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-0.762" y1="0" x2="0" y2="-0.762" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-0.762" x2="0" y2="-1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB" prefix="X">
<description>SMD micro USB connector as found in the fablab inventory. 
Three footprint variants included: 
&lt;ol&gt;
&lt;li&gt;609-4613-1-ND used by Jake
&lt;li&gt; original, as described by manufacturer's datasheet
&lt;li&gt; for milling with the 1/100" bit
&lt;li&gt; for milling with the 1/64" bit
&lt;/ol&gt;
&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="_1/100" package="DX4R005HJ5_100">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ORIG" package="DX4R005HJ5">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1/64" package="DX4R005HJ5_64">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="USB_MICRO_609-4613-1-ND">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CASTELLATED_PIN" prefix="J">
<gates>
<gate name="G$1" symbol="CONN_01_CASTELLATED" x="0" y="0"/>
</gates>
<devices>
<device name="0.1" package="CASTELLATED_PIN_0.1">
<connects>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.05" package="CASTELLATED_PIN_0.05">
<connects>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X5-PTH-1.27MM-NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.762" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.762" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.762" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.762" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.762" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.762" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.762" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.762" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.762" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.762" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="51"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.635" y1="-1.905" x2="0.635" y2="-1.905" width="0.254" layer="21"/>
<wire x1="5.2" y1="1.6" x2="-5.2" y2="1.6" width="0.127" layer="51"/>
<wire x1="-5.2" y1="1.6" x2="-5.2" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-5.2" y1="-1.6" x2="5.2" y2="-1.6" width="0.127" layer="51"/>
<wire x1="5.2" y1="-1.6" x2="5.2" y2="1.6" width="0.127" layer="51"/>
</package>
<package name="2X5-PTH-1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.635" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.635" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.635" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="21"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SMD-1.27MM">
<description>Shrouded SMD connector for JTAG and SWD applications.</description>
<smd name="6" x="0" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="8" x="-1.27" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="10" x="-2.54" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="4" x="1.27" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="2" x="2.54" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="1" x="2.54" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="5" x="0" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="7" x="-1.27" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="9" x="-2.54" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<rectangle x1="-1.0575" y1="-1.9625" x2="1.0575" y2="-1.5525" layer="51" rot="R270"/>
<wire x1="5.55" y1="-1.7" x2="-5.55" y2="-1.7" width="0.1524" layer="51"/>
<wire x1="-5.55" y1="-1.7" x2="-5.55" y2="1.7" width="0.1524" layer="51"/>
<wire x1="-5.55" y1="1.7" x2="5.55" y2="1.7" width="0.1524" layer="51"/>
<wire x1="5.55" y1="1.7" x2="5.55" y2="-1.7" width="0.1524" layer="51"/>
<rectangle x1="-2.3275" y1="-1.9625" x2="-0.2125" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="-3.5975" y1="-1.9625" x2="-1.4825" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="0.2125" y1="-1.9625" x2="2.3275" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="1.4825" y1="-1.9625" x2="3.5975" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="1.4825" y1="1.5525" x2="3.5975" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="0.2125" y1="1.5525" x2="2.3275" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="-1.0575" y1="1.5525" x2="1.0575" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="-2.3275" y1="1.5525" x2="-0.2125" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="-3.5975" y1="1.5525" x2="-1.4825" y2="1.9625" layer="51" rot="R90"/>
<wire x1="-3.2" y1="2.5" x2="-6.3" y2="2.5" width="0.2032" layer="51"/>
<wire x1="-6.3" y1="2.5" x2="-6.3" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-6.3" y1="-2.5" x2="-3.2" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-2.5" x2="6.3" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="6.3" y1="-2.5" x2="6.3" y2="2.5" width="0.2032" layer="51"/>
<wire x1="6.3" y1="2.5" x2="3.2" y2="2.5" width="0.2032" layer="51"/>
<wire x1="0.6" y1="2.9" x2="0.6" y2="3.4" width="0.2032" layer="21"/>
<wire x1="0.6" y1="3.4" x2="-0.6" y2="3.4" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="3.4" x2="-0.6" y2="2.9" width="0.2032" layer="21"/>
<circle x="3.6" y="3.1" radius="0.1" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.127" layer="51"/>
<wire x1="3.175" y1="1.905" x2="3.175" y2="-1.905" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CORTEX_DEBUG">
<description>&lt;h3&gt;Cortex Debug Connector&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="VCC" x="-15.24" y="5.08" length="short"/>
<pin name="GND@3" x="-15.24" y="2.54" length="short"/>
<pin name="GND@5" x="-15.24" y="0" length="short"/>
<pin name="KEY" x="-15.24" y="-2.54" length="short"/>
<pin name="GNDDTCT" x="-15.24" y="-5.08" length="short"/>
<pin name="!RESET" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="NC/TDI" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="SWO/TDO" x="15.24" y="0" length="short" rot="R180"/>
<pin name="SWDCLK/TCK" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="SWDIO/TMS" x="15.24" y="5.08" length="short" rot="R180"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<text x="-12.7" y="7.874" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-12.7" y="-9.906" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CORTEX_JTAG_DEBUG" prefix="J">
<description>&lt;h3&gt;Cortex Debug Connector - 10 pin&lt;/h3&gt;
&lt;p&gt;Supports JTAG debug, Serial Wire debug, and Serial Wire Viewer.
PTH and SMD connector options available.&lt;/p&gt;
&lt;p&gt; &lt;ul&gt;&lt;a href=”http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf”&gt;General Connector Information&lt;/a&gt;
&lt;p&gt;&lt;b&gt; Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”http://www.digikey.com/product-detail/en/cnc-tech/3220-10-0100-00/1175-1627-ND/3883661”&gt;PTH Connector&lt;/a&gt; -via Digi-Key&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13229”&gt;SparkFun PSoc&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13810”&gt;SparkFun T&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="J1" symbol="CORTEX_DEBUG" x="0" y="0"/>
</gates>
<devices>
<device name="_PTH_NS" package="2X5-PTH-1.27MM-NO_SILK">
<connects>
<connect gate="J1" pin="!RESET" pad="10"/>
<connect gate="J1" pin="GND@3" pad="3"/>
<connect gate="J1" pin="GND@5" pad="5"/>
<connect gate="J1" pin="GNDDTCT" pad="9"/>
<connect gate="J1" pin="KEY" pad="7"/>
<connect gate="J1" pin="NC/TDI" pad="8"/>
<connect gate="J1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="J1" pin="SWDIO/TMS" pad="2"/>
<connect gate="J1" pin="SWO/TDO" pad="6"/>
<connect gate="J1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PTH" package="2X5-PTH-1.27MM">
<connects>
<connect gate="J1" pin="!RESET" pad="10"/>
<connect gate="J1" pin="GND@3" pad="3"/>
<connect gate="J1" pin="GND@5" pad="5"/>
<connect gate="J1" pin="GNDDTCT" pad="9"/>
<connect gate="J1" pin="KEY" pad="7"/>
<connect gate="J1" pin="NC/TDI" pad="8"/>
<connect gate="J1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="J1" pin="SWDIO/TMS" pad="2"/>
<connect gate="J1" pin="SWO/TDO" pad="6"/>
<connect gate="J1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD" package="2X5-SMD-1.27MM">
<connects>
<connect gate="J1" pin="!RESET" pad="10"/>
<connect gate="J1" pin="GND@3" pad="3"/>
<connect gate="J1" pin="GND@5" pad="5"/>
<connect gate="J1" pin="GNDDTCT" pad="9"/>
<connect gate="J1" pin="KEY" pad="7"/>
<connect gate="J1" pin="NC/TDI" pad="8"/>
<connect gate="J1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="J1" pin="SWDIO/TMS" pad="2"/>
<connect gate="J1" pin="SWO/TDO" pad="6"/>
<connect gate="J1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14503" constant="no"/>
<attribute name="VALUE" value="JTAG" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;, 5 lead</description>
<wire x1="-1.544" y1="0.713" x2="1.544" y2="0.713" width="0.1524" layer="51"/>
<wire x1="1.544" y1="0.713" x2="1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="1.544" y1="-0.712" x2="-1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="-1.544" y1="-0.712" x2="-1.544" y2="0.713" width="0.1524" layer="51"/>
<smd name="5" x="-0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<text x="-1.778" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.048" y="-1.778" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
<wire x1="-1.5" y1="-1.9" x2="-1.5" y2="-1.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="VREG-AP2112">
<pin name="VIN" x="-12.7" y="2.54" length="middle"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VREG-AP2112" prefix="U">
<gates>
<gate name="G$1" symbol="VREG-AP2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.6" x2="1" y2="0.6" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.032" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="TACT-SWITCH-SIDE">
<smd name="P$1" x="-1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$2" x="1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$3" x="-1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$4" x="1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<wire x1="-0.9" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0.9" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0.9" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.45" x2="1.75" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-1.75" y1="1.6" x2="-1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="0" y2="1.6" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="1" y2="1.6" width="0.127" layer="21"/>
<wire x1="1" y1="1.6" x2="1.75" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="-1" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1" y1="2.3" x2="1" y2="2.3" width="0.127" layer="21"/>
<wire x1="1" y1="2.3" x2="1" y2="1.6" width="0.127" layer="21"/>
</package>
<package name="744777920-INDUCTOR">
<smd name="P$1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="P$2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="-4" y1="0" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="4" y1="3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-4" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-4" y1="-3" x2="-4" y2="0" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-4" x2="4" y2="4" layer="39"/>
<text x="5.08" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="SPM6530-IND">
<smd name="1" x="0" y="2.775" dx="3.4" dy="1.85" layer="1"/>
<smd name="2" x="0" y="-2.775" dx="3.4" dy="1.85" layer="1"/>
<wire x1="-3.25" y1="3.85" x2="-3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.85" x2="3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.85" x2="3.25" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.85" x2="-3.25" y2="3.85" width="0.127" layer="21"/>
<text x="3.81" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="IHLP-5050FD-01-IND">
<smd name="1" x="0" y="5.4102" dx="4.953" dy="2.9464" layer="1"/>
<smd name="2" x="0" y="-5.4102" dx="4.953" dy="2.9464" layer="1"/>
<wire x1="6.4516" y1="6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="6.4516" y1="6.604" x2="3.81" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.604" x2="-6.4516" y2="6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="6.604" x2="-6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="-6.604" x2="-3.81" y2="-6.604" width="0.127" layer="21"/>
<text x="5.08" y="7.62" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="-8.89" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="7443340330-IND">
<smd name="P$1" x="0" y="3.35" dx="3" dy="2.3" layer="1"/>
<smd name="P$2" x="0" y="-3.35" dx="3" dy="2.3" layer="1"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<text x="3" y="5" size="1.016" layer="25">&gt;NAME</text>
<text x="3" y="-6" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.15" y1="2.95" x2="1.15" y2="4.45" layer="51"/>
<rectangle x1="-1.15" y1="-4.45" x2="1.15" y2="-2.95" layer="51"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="8X8-IND">
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="744029100-IND">
<smd name="1" x="0" y="1.1" dx="3.2" dy="1" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="3.2" dy="1" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447709470-IND">
<smd name="1" x="0" y="4.95" dx="5.4" dy="2.9" layer="1"/>
<smd name="2" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<text x="-7" y="8" size="1.27" layer="25">&gt;NAME</text>
<text x="-7" y="-9" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447789002-IND">
<smd name="1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="NRS5020">
<smd name="P$1" x="-1.8" y="0" dx="1.5" dy="4" layer="1"/>
<smd name="P$2" x="1.8" y="0" dx="1.5" dy="4" layer="1"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="51"/>
</package>
<package name="DIPSWITCH-8">
<smd name="P$1" x="-4.445" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$2" x="-3.175" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$3" x="-1.905" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$4" x="-0.635" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$5" x="0.635" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$6" x="1.905" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$7" x="3.175" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$8" x="4.445" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$9" x="4.445" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$10" x="3.175" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$11" x="1.905" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$12" x="0.635" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$13" x="-0.635" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$14" x="-1.905" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$15" x="-3.175" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$16" x="-4.445" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<rectangle x1="-5.6642" y1="-2.8956" x2="5.6642" y2="2.8956" layer="51"/>
<text x="-5.08" y="0" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="2-SMD-3.2X1.5MM">
<smd name="P$1" x="-1.25" y="0" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<smd name="P$2" x="1.25" y="0" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<wire x1="-0.6" y1="0.9" x2="0.6" y2="0.9" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.9" x2="0.6" y2="-0.9" width="0.127" layer="51"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="4-SMD-3.2X2.5">
<smd name="P$1" x="-1.1" y="-0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$2" x="1.1" y="-0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$3" x="1.1" y="0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$4" x="-1.1" y="0.8" dx="1.4" dy="1.2" layer="1"/>
<rectangle x1="-1.6" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
<circle x="-2" y="-1.6" radius="0.14141875" width="0.127" layer="21"/>
</package>
<package name="DIPSWITCH-8-0100">
<smd name="P$1" x="-8.89" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$2" x="-6.35" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$3" x="-3.81" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$4" x="-1.27" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$5" x="1.27" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$6" x="3.81" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$7" x="6.35" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$8" x="8.89" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$9" x="8.89" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$10" x="6.35" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$11" x="3.81" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$12" x="1.27" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$13" x="-1.27" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$14" x="-3.81" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$15" x="-6.35" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$16" x="-8.89" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<rectangle x1="-10.895" y1="-3.34" x2="10.895" y2="3.34" layer="51"/>
<text x="-9.525" y="1.27" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="6.35" y="-5.08" size="1.27" layer="97" rot="R90">&gt;PACKAGE</text>
</symbol>
<symbol name="DIPSWITCH-8">
<pin name="1" x="-12.7" y="7.62" length="middle"/>
<pin name="2" x="-12.7" y="5.08" length="middle"/>
<pin name="3" x="-12.7" y="2.54" length="middle"/>
<pin name="4" x="-12.7" y="0" length="middle"/>
<pin name="5" x="-12.7" y="-2.54" length="middle"/>
<pin name="6" x="-12.7" y="-5.08" length="middle"/>
<pin name="7" x="-12.7" y="-7.62" length="middle"/>
<pin name="8" x="-12.7" y="-10.16" length="middle"/>
<pin name="9" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="10" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="11" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="12" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="13" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="14" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="15" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="16" x="12.7" y="7.62" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="RESONATOR">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CRYSTAL-MHZ">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-7.62" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="TACT-SWITCH-SIDE">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-744777920" package="744777920-INDUCTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
</technology>
</technologies>
</device>
<device name="-SPM6530" package="SPM6530-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IHLP-5050FD-01" package="IHLP-5050FD-01-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7443340330" package="7443340330-IND">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="7443340330"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
</technology>
</technologies>
</device>
<device name="-744778002" package="8X8-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-744029100" package="744029100-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447709470" package="7447709470-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447789002" package="7447789002-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="NRS5020">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIPSWITCH-8" prefix="J">
<gates>
<gate name="G$1" symbol="DIPSWITCH-8" x="0" y="0"/>
</gates>
<devices>
<device name="MICRO" package="DIPSWITCH-8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="10" pad="P$10"/>
<connect gate="G$1" pin="11" pad="P$11"/>
<connect gate="G$1" pin="12" pad="P$12"/>
<connect gate="G$1" pin="13" pad="P$13"/>
<connect gate="G$1" pin="14" pad="P$14"/>
<connect gate="G$1" pin="15" pad="P$15"/>
<connect gate="G$1" pin="16" pad="P$16"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
<connect gate="G$1" pin="7" pad="P$7"/>
<connect gate="G$1" pin="8" pad="P$8"/>
<connect gate="G$1" pin="9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0100" package="DIPSWITCH-8-0100">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="10" pad="P$10"/>
<connect gate="G$1" pin="11" pad="P$11"/>
<connect gate="G$1" pin="12" pad="P$12"/>
<connect gate="G$1" pin="13" pad="P$13"/>
<connect gate="G$1" pin="14" pad="P$14"/>
<connect gate="G$1" pin="15" pad="P$15"/>
<connect gate="G$1" pin="16" pad="P$16"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
<connect gate="G$1" pin="7" pad="P$7"/>
<connect gate="G$1" pin="8" pad="P$8"/>
<connect gate="G$1" pin="9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KHZ-CRYSTAL" prefix="Y">
<gates>
<gate name="G$1" symbol="RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2-SMD-3.2X1.5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MHZ-CRYSTAL" prefix="Y">
<gates>
<gate name="G$1" symbol="CRYSTAL-MHZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4-SMD-3.2X2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$3"/>
<connect gate="G$1" pin="GND" pad="P$2 P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="comm">
<packages>
<package name="SOP65P637X110-14N">
<rectangle x1="-0.901" y1="-0.9328" x2="0.901" y2="0.9328" layer="31"/>
<circle x="-4.355" y="2.36" radius="0.1" width="0.2" layer="21"/>
<circle x="-4.355" y="2.36" radius="0.1" width="0.2" layer="51"/>
<wire x1="-2.25" y1="2.55" x2="2.25" y2="2.55" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.55" x2="2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.55" x2="2.25" y2="2.55" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.55" x2="2.25" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.55" x2="-2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.55" x2="2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-3.865" y1="2.8" x2="3.865" y2="2.8" width="0.05" layer="39"/>
<wire x1="-3.865" y1="-2.8" x2="3.865" y2="-2.8" width="0.05" layer="39"/>
<wire x1="-3.865" y1="2.8" x2="-3.865" y2="-2.8" width="0.05" layer="39"/>
<wire x1="3.865" y1="2.8" x2="3.865" y2="-2.8" width="0.05" layer="39"/>
<text x="-3.73" y="-2.727" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<text x="-3.73" y="2.727" size="1.27" layer="25">&gt;NAME</text>
<smd name="1" x="-2.875" y="1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="2" x="-2.875" y="1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="3" x="-2.875" y="0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="4" x="-2.875" y="0" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="5" x="-2.875" y="-0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="6" x="-2.875" y="-1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="7" x="-2.875" y="-1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="8" x="2.875" y="-1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="9" x="2.875" y="-1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="10" x="2.875" y="-0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="11" x="2.875" y="0" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="12" x="2.875" y="0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="13" x="2.875" y="1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="14" x="2.875" y="1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="15" x="0" y="0" dx="2.85" dy="2.95" layer="1" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="MAX13450EAUDT">
<wire x1="-12.7" y1="35.56" x2="12.7" y2="35.56" width="0.41" layer="94"/>
<wire x1="12.7" y1="35.56" x2="12.7" y2="-35.56" width="0.41" layer="94"/>
<wire x1="12.7" y1="-35.56" x2="-12.7" y2="-35.56" width="0.41" layer="94"/>
<wire x1="-12.7" y1="-35.56" x2="-12.7" y2="35.56" width="0.41" layer="94"/>
<text x="-12.7" y="36.56" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-12.7" y="-39.56" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="DI" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="!RE" x="-17.78" y="20.32" length="middle"/>
<pin name="!TERM" x="-17.78" y="15.24" length="middle"/>
<pin name="A" x="-17.78" y="10.16" length="middle"/>
<pin name="B" x="-17.78" y="5.08" length="middle"/>
<pin name="DE" x="-17.78" y="0" length="middle"/>
<pin name="RO" x="-17.78" y="-5.08" length="middle"/>
<pin name="SRL" x="-17.78" y="-10.16" length="middle"/>
<pin name="TERM100" x="-17.78" y="-15.24" length="middle"/>
<pin name="Y" x="-17.78" y="-20.32" length="middle"/>
<pin name="Z" x="-17.78" y="-25.4" length="middle"/>
<pin name="VCC" x="17.78" y="33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VL" x="17.78" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="EPAD" x="17.78" y="-30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-33.02" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MAX13450EAUDT" prefix="U">
<gates>
<gate name="A" symbol="MAX13450EAUDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P637X110-14N">
<connects>
<connect gate="A" pin="!RE" pad="2"/>
<connect gate="A" pin="!TERM" pad="5"/>
<connect gate="A" pin="A" pad="8"/>
<connect gate="A" pin="B" pad="9"/>
<connect gate="A" pin="DE" pad="1"/>
<connect gate="A" pin="DI" pad="3"/>
<connect gate="A" pin="EPAD" pad="15"/>
<connect gate="A" pin="GND" pad="11"/>
<connect gate="A" pin="RO" pad="7"/>
<connect gate="A" pin="SRL" pad="14"/>
<connect gate="A" pin="TERM100" pad="13"/>
<connect gate="A" pin="VCC" pad="4"/>
<connect gate="A" pin="VL" pad="6"/>
<connect gate="A" pin="Y" pad="10"/>
<connect gate="A" pin="Z" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" 1/1 Transceiver Full RS422, RS485 14-TSSOP-EP "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="MAX13450EAUD+T-ND"/>
<attribute name="DIGIKEY-PURCHASE-URL" value="https://snapeda.com/shop?store=DigiKey&amp;id=694117"/>
<attribute name="MF" value="Maxim Integrated"/>
<attribute name="MP" value="MAX13450EAUD+"/>
<attribute name="PACKAGE" value="TSSOP-14 Maxim"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.0778" y1="0.2818" x2="0.1278" y2="0" width="0.127" layer="21"/>
<wire x1="0.1278" y1="0" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="0.2818" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
</package>
<package name="LED-5630">
<smd name="P$1" x="0.35" y="0" dx="2.54" dy="1.17" layer="1" thermals="no"/>
<smd name="P$2" x="2.64" y="0.6" dx="0.52" dy="0.9" layer="1" thermals="no"/>
<smd name="P$3" x="2.64" y="-0.6" dx="0.52" dy="0.9" layer="1" thermals="no"/>
<smd name="P$4" x="-2.64" y="0.6" dx="0.52" dy="0.9" layer="1" thermals="no"/>
<smd name="P$5" x="-2.64" y="-0.6" dx="0.52" dy="0.9" layer="1" thermals="no"/>
<rectangle x1="-2.5" y1="-1.5" x2="2.5" y2="1.5" layer="51"/>
<wire x1="0" y1="1" x2="0" y2="1.5" width="0.127" layer="21"/>
<wire x1="0" y1="-1" x2="0" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.3" y1="0.7" x2="-0.4" y2="1.6" width="0.127" layer="21"/>
<wire x1="-0.4" y1="1.6" x2="3" y2="1.6" width="0.127" layer="21"/>
<wire x1="3" y1="1.6" x2="3" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3" y1="-1.6" x2="-0.4" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-1.6" x2="-1.3" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.7" x2="-1.3" y2="0.7" width="0.127" layer="21"/>
<circle x="-2.9" y="-1.5" radius="0.1" width="0.127" layer="21"/>
<text x="-4.4" y="1.8" size="1.27" layer="21">pls update for big pads, track dot</text>
</package>
<package name="0402-D">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1" y1="-0.2" x2="-1" y2="0.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5630" package="LED-5630">
<connects>
<connect gate="G$1" pin="A" pad="P$4 P$5"/>
<connect gate="G$1" pin="C" pad="P$1 P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-D">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="connector" deviceset="USB" device=""/>
<part name="J11" library="SparkFun-Connectors" deviceset="CORTEX_JTAG_DEBUG" device="_PTH_NS" value="CORTEX_JTAG_DEBUG_PTH_NS"/>
<part name="C7" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="R6" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="R1" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="S1" library="passives" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="C2" library="passives" deviceset="CAP" device="0402" value="1uF 10v"/>
<part name="C1" library="passives" deviceset="CAP" device="0603-CAP" value="10uF 10v"/>
<part name="L1" library="passives" deviceset="INDUCTOR" device="-0805" value="10uH"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="C18" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="C10" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="C15" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="C9" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="U3" library="comm" deviceset="MAX13450EAUDT" device=""/>
<part name="C16" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="C11" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="C14" library="passives" deviceset="CAP" device="0402" value="1uf 10v"/>
<part name="C12" library="passives" deviceset="CAP" device="0402" value="1uF 10v"/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="P+7" library="supply1" deviceset="+5V" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="J10" library="passives" deviceset="DIPSWITCH-8" device="MICRO"/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="R5" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="R7" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="R8" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="U1" library="power" deviceset="VREG-AP2112" device=""/>
<part name="P+9" library="supply1" deviceset="+5V" device=""/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="C3" library="passives" deviceset="CAP" device="0603-CAP" value="10uF 10v"/>
<part name="C6" library="passives" deviceset="CAP" device="0402" value="1uF 10v"/>
<part name="C4" library="passives" deviceset="CAP" device="0603-CAP" value="10uF 10v"/>
<part name="C5" library="passives" deviceset="CAP" device="0402" value="10pF 10v"/>
<part name="Y1" library="passives" deviceset="KHZ-CRYSTAL" device=""/>
<part name="Y2" library="passives" deviceset="MHZ-CRYSTAL" device=""/>
<part name="C8" library="passives" deviceset="CAP" device="0402" value="10pF 10v"/>
<part name="C17" library="passives" deviceset="CAP" device="0402" value="10pF 10v"/>
<part name="C13" library="passives" deviceset="CAP" device="0402" value="10pF 10v"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="D1" library="lights" deviceset="LED" device="0402"/>
<part name="D2" library="lights" deviceset="LED" device="0402"/>
<part name="R3" library="passives" deviceset="RESISTOR" device="0402" value="470R"/>
<part name="R4" library="passives" deviceset="RESISTOR" device="0402" value="470R"/>
<part name="D3" library="lights" deviceset="LED" device="0402"/>
<part name="R2" library="passives" deviceset="RESISTOR" device="0402" value="470R"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="P+11" library="supply1" deviceset="+5V" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="U2" library="microcontrollers" deviceset="ATSAMD51J" device="QFN64"/>
<part name="J24" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J25" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J26" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J27" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J28" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J29" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J30" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J31" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J32" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J33" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J34" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J35" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J36" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J37" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J38" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J39" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J40" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J41" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J23" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J21" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J19" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J17" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J15" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J14" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J13" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J12" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J9" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J8" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J7" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J6" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J5" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J4" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V8" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V9" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V10" library="supply1" deviceset="+3V3" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="J3" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J2" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="J1" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="+3V11" library="supply1" deviceset="+3V3" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="J16" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J18" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J20" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="J22" library="connector" deviceset="CASTELLATED_PIN" device="0.05"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="205.74" y1="134.62" x2="205.74" y2="127" width="0.1524" layer="97"/>
<text x="208.28" y="129.54" size="1.778" layer="97">SER0</text>
<wire x1="205.74" y1="124.46" x2="205.74" y2="116.84" width="0.1524" layer="97"/>
<text x="208.28" y="119.38" size="1.778" layer="97">SER0|2</text>
<wire x1="205.74" y1="93.98" x2="205.74" y2="86.36" width="0.1524" layer="97"/>
<text x="208.28" y="88.9" size="1.778" layer="97">SER3|5</text>
<wire x1="203.2" y1="96.52" x2="203.2" y2="104.14" width="0.1524" layer="97"/>
<text x="205.74" y="96.52" size="1.778" layer="97">SER1:RS485</text>
<wire x1="205.74" y1="27.94" x2="205.74" y2="35.56" width="0.1524" layer="97"/>
<text x="208.28" y="30.48" size="1.778" layer="97">SER4</text>
<wire x1="218.44" y1="124.46" x2="218.44" y2="111.76" width="0.1524" layer="97"/>
<text x="220.98" y="114.3" size="1.778" layer="97">TCC0:0-3,6-7</text>
<wire x1="218.44" y1="40.64" x2="218.44" y2="38.1" width="0.1524" layer="97"/>
<text x="220.98" y="38.1" size="1.778" layer="97">TCC0:4-5</text>
<wire x1="241.3" y1="139.7" x2="241.3" y2="111.76" width="0.1524" layer="97"/>
<text x="243.84" y="139.7" size="1.778" layer="97">DAC-0</text>
<text x="243.84" y="132.08" size="1.778" layer="97">DAC-1</text>
<text x="243.84" y="137.16" size="1.778" layer="97">DAC-VREF</text>
<text x="246.38" y="114.3" size="1.778" layer="97">ADC0</text>
<wire x1="241.3" y1="50.8" x2="241.3" y2="48.26" width="0.1524" layer="97"/>
<text x="246.38" y="48.26" size="1.778" layer="97">ADC1</text>
<wire x1="218.44" y1="88.9" x2="218.44" y2="86.36" width="0.1524" layer="97"/>
<text x="220.98" y="86.36" size="1.778" layer="97">CANBUS</text>
<wire x1="205.74" y1="17.78" x2="205.74" y2="25.4" width="0.1524" layer="97"/>
<text x="208.28" y="20.32" size="1.778" layer="97">SER5</text>
</plain>
<instances>
<instance part="X1" gate="G$1" x="15.24" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="13.335" y="65.405" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="13.335" y="52.705" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="J11" gate="J1" x="25.4" y="33.02" smashed="yes">
<attribute name="NAME" x="12.7" y="40.894" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C7" gate="G$1" x="91.44" y="15.24" smashed="yes">
<attribute name="NAME" x="92.964" y="18.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.964" y="13.081" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="91.44" y="48.26" smashed="yes" rot="R270">
<attribute name="NAME" x="92.9386" y="52.07" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="88.138" y="52.07" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R1" gate="G$1" x="60.96" y="48.26" smashed="yes" rot="R270">
<attribute name="NAME" x="62.4586" y="52.07" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="57.658" y="52.07" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND1" gate="1" x="78.74" y="5.08" smashed="yes">
<attribute name="VALUE" x="76.2" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="91.44" y="60.96" smashed="yes">
<attribute name="VALUE" x="88.9" y="55.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S1" gate="G$1" x="76.2" y="20.32" smashed="yes">
<attribute name="NAME" x="69.85" y="17.78" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="72.39" y="23.495" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V2" gate="G$1" x="60.96" y="60.96" smashed="yes">
<attribute name="VALUE" x="58.42" y="55.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="91.44" y="93.98" smashed="yes">
<attribute name="NAME" x="92.964" y="96.901" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.964" y="91.821" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="78.74" y="93.98" smashed="yes">
<attribute name="NAME" x="80.264" y="96.901" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.264" y="91.821" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="80.264" y="89.916" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="80.264" y="88.138" size="1.27" layer="97"/>
<attribute name="TYPE" x="80.264" y="86.36" size="1.27" layer="97"/>
</instance>
<instance part="L1" gate="G$1" x="91.44" y="114.3" smashed="yes" rot="R270">
<attribute name="NAME" x="86.36" y="115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="86.36" y="110.49" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="86.36" y="107.95" size="1.27" layer="97"/>
</instance>
<instance part="GND2" gate="1" x="78.74" y="81.28" smashed="yes">
<attribute name="VALUE" x="76.2" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="78.74" y="139.7" smashed="yes">
<attribute name="NAME" x="80.264" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.264" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="66.04" y="139.7" smashed="yes">
<attribute name="NAME" x="67.564" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.564" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="53.34" y="139.7" smashed="yes">
<attribute name="NAME" x="54.864" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="54.864" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="40.64" y="139.7" smashed="yes">
<attribute name="NAME" x="42.164" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="42.164" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="40.64" y="127" smashed="yes">
<attribute name="VALUE" x="38.1" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="+3V3" gate="G$1" x="40.64" y="152.4" smashed="yes">
<attribute name="VALUE" x="38.1" y="147.32" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U3" gate="A" x="373.38" y="45.72" smashed="yes">
<attribute name="NAME" x="360.68" y="82.28" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="360.68" y="6.16" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="C16" gate="G$1" x="396.24" y="71.12" smashed="yes">
<attribute name="NAME" x="397.764" y="74.041" size="1.778" layer="95"/>
<attribute name="VALUE" x="397.764" y="68.961" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="434.34" y="73.66" smashed="yes">
<attribute name="NAME" x="435.864" y="76.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="435.864" y="71.501" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="396.24" y="5.08" smashed="yes">
<attribute name="VALUE" x="393.7" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="411.48" y="71.12" smashed="yes">
<attribute name="NAME" x="413.004" y="74.041" size="1.778" layer="95"/>
<attribute name="VALUE" x="413.004" y="68.961" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="449.58" y="73.66" smashed="yes">
<attribute name="NAME" x="451.104" y="76.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="451.104" y="71.501" size="1.778" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="421.64" y="76.2" smashed="yes" rot="R270">
<attribute name="VALUE" x="421.64" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="P+7" gate="1" x="459.74" y="78.74" smashed="yes" rot="R270">
<attribute name="VALUE" x="462.28" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="396.24" y="58.42" smashed="yes">
<attribute name="VALUE" x="393.7" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="434.34" y="60.96" smashed="yes">
<attribute name="VALUE" x="431.8" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="J10" gate="G$1" x="441.96" y="142.24" smashed="yes"/>
<instance part="+3V7" gate="G$1" x="477.52" y="149.86" smashed="yes" rot="R270">
<attribute name="VALUE" x="472.44" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="467.36" y="149.86" smashed="yes">
<attribute name="NAME" x="463.55" y="151.3586" size="1.778" layer="95"/>
<attribute name="VALUE" x="463.55" y="146.558" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="332.74" y="30.48" smashed="yes">
<attribute name="NAME" x="328.93" y="26.8986" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="22.098" size="1.778" layer="96"/>
</instance>
<instance part="GND16" gate="1" x="322.58" y="30.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="33.02" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R8" gate="G$1" x="342.9" y="35.56" smashed="yes">
<attribute name="NAME" x="339.09" y="37.0586" size="1.778" layer="95"/>
<attribute name="VALUE" x="339.09" y="32.258" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="317.5" y="35.56" smashed="yes" rot="R270">
<attribute name="VALUE" x="314.96" y="38.1" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U1" gate="G$1" x="38.1" y="111.76" smashed="yes">
<attribute name="NAME" x="35.56" y="119.38" size="1.27" layer="95"/>
<attribute name="VALUE" x="40.64" y="104.14" size="1.27" layer="96"/>
</instance>
<instance part="P+9" gate="1" x="7.62" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="5.08" y="114.3" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V5" gate="G$1" x="71.12" y="114.3" smashed="yes" rot="R270">
<attribute name="VALUE" x="66.04" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="38.1" y="93.98" smashed="yes">
<attribute name="VALUE" x="35.56" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="53.34" y="109.22" smashed="yes">
<attribute name="NAME" x="54.864" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="54.864" y="107.061" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="60.96" y="109.22" smashed="yes">
<attribute name="NAME" x="62.484" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="62.484" y="107.061" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="20.32" y="109.22" smashed="yes">
<attribute name="NAME" x="21.844" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="21.844" y="107.061" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="307.34" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="304.419" y="151.384" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="309.499" y="151.384" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="Y1" gate="G$1" x="292.1" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="293.116" y="144.78" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="289.56" y="144.78" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="Y2" gate="G$1" x="292.1" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="291.084" y="121.92" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="294.64" y="121.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="304.8" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="307.721" y="143.256" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="302.641" y="143.256" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C17" gate="G$1" x="307.34" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="310.261" y="115.316" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="305.181" y="115.316" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C13" gate="G$1" x="309.88" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="306.959" y="123.444" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="312.039" y="123.444" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="320.04" y="147.32" smashed="yes" rot="R90">
<attribute name="VALUE" x="322.58" y="144.78" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND21" gate="1" x="320.04" y="119.38" smashed="yes" rot="R90">
<attribute name="VALUE" x="322.58" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D1" gate="G$1" x="353.06" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="351.028" y="146.304" size="1.778" layer="95"/>
<attribute name="VALUE" x="351.028" y="144.145" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="365.76" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="363.728" y="143.764" size="1.778" layer="95"/>
<attribute name="VALUE" x="363.728" y="141.605" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="378.46" y="149.86" smashed="yes">
<attribute name="NAME" x="374.65" y="151.3586" size="1.778" layer="95"/>
<attribute name="VALUE" x="374.65" y="146.558" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="391.16" y="147.32" smashed="yes">
<attribute name="NAME" x="387.35" y="148.8186" size="1.778" layer="95"/>
<attribute name="VALUE" x="387.35" y="144.018" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="378.46" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="376.428" y="131.064" size="1.778" layer="95"/>
<attribute name="VALUE" x="376.428" y="128.905" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="391.16" y="134.62" smashed="yes">
<attribute name="NAME" x="387.35" y="136.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="387.35" y="131.318" size="1.778" layer="96"/>
</instance>
<instance part="GND22" gate="1" x="365.76" y="134.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="363.22" y="137.16" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+11" gate="1" x="40.64" y="60.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="43.18" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="35.56" y="63.5" smashed="yes" rot="R90">
<attribute name="VALUE" x="38.1" y="60.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U2" gate="G$1" x="139.7" y="144.78" smashed="yes">
<attribute name="NAME" x="134.62" y="152.4" size="1.778" layer="95"/>
<attribute name="VALUE" x="134.62" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="J24" gate="G$1" x="299.72" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="71.12" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J25" gate="G$1" x="299.72" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="68.58" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J26" gate="G$1" x="299.72" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="66.04" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J27" gate="G$1" x="299.72" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="63.5" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J28" gate="G$1" x="299.72" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="60.96" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J29" gate="G$1" x="299.72" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="58.42" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J30" gate="G$1" x="299.72" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="55.88" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J31" gate="G$1" x="299.72" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="53.34" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J32" gate="G$1" x="299.72" y="50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="50.8" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J33" gate="G$1" x="299.72" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="45.72" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J34" gate="G$1" x="299.72" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="43.18" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J35" gate="G$1" x="299.72" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="40.64" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J36" gate="G$1" x="299.72" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="38.1" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J37" gate="G$1" x="299.72" y="35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="35.56" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J38" gate="G$1" x="299.72" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="33.02" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J39" gate="G$1" x="299.72" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="30.48" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J40" gate="G$1" x="299.72" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="27.94" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J41" gate="G$1" x="299.72" y="25.4" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="25.4" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J23" gate="G$1" x="299.72" y="20.32" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="20.32" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J21" gate="G$1" x="299.72" y="17.78" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="17.78" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J19" gate="G$1" x="299.72" y="15.24" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="15.24" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J17" gate="G$1" x="299.72" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="12.7" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J15" gate="G$1" x="299.72" y="10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="10.16" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J14" gate="G$1" x="299.72" y="7.62" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="7.62" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J13" gate="G$1" x="299.72" y="5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="5.08" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J12" gate="G$1" x="299.72" y="2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="2.54" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J9" gate="G$1" x="299.72" y="-2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-2.54" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J8" gate="G$1" x="299.72" y="-5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-5.08" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J7" gate="G$1" x="299.72" y="-7.62" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-7.62" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J6" gate="G$1" x="299.72" y="-10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-10.16" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J5" gate="G$1" x="299.72" y="-12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-12.7" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J4" gate="G$1" x="299.72" y="-15.24" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-15.24" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="GND4" gate="1" x="271.78" y="12.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="15.24" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V4" gate="G$1" x="403.86" y="149.86" smashed="yes" rot="R270">
<attribute name="VALUE" x="406.4" y="149.86" size="1.778" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="403.86" y="147.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="406.4" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="+3V9" gate="G$1" x="403.86" y="134.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="406.4" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="+3V10" gate="G$1" x="274.32" y="5.08" smashed="yes" rot="R90">
<attribute name="VALUE" x="271.78" y="5.08" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+1" gate="1" x="274.32" y="2.54" smashed="yes" rot="R90">
<attribute name="VALUE" x="271.78" y="2.54" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J3" gate="G$1" x="299.72" y="-17.78" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-17.78" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J2" gate="G$1" x="299.72" y="-20.32" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-20.32" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="GND5" gate="1" x="271.78" y="60.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="63.5" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND6" gate="1" x="271.78" y="35.56" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="38.1" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND7" gate="1" x="271.78" y="-12.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="-10.16" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J1" gate="G$1" x="299.72" y="-22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="-22.86" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="+3V11" gate="G$1" x="0" y="43.18" smashed="yes">
<attribute name="VALUE" x="-2.54" y="38.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND8" gate="1" x="2.54" y="20.32" smashed="yes">
<attribute name="VALUE" x="0" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="J16" gate="G$1" x="299.72" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="86.36" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J18" gate="G$1" x="299.72" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="83.82" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J20" gate="G$1" x="299.72" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="81.28" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="J22" gate="G$1" x="299.72" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="300.99" y="78.74" size="1.778" layer="95" font="vector" align="center-left"/>
</instance>
<instance part="GND9" gate="1" x="335.28" y="66.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="332.74" y="68.58" size="1.778" layer="96" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="USBDM" class="0">
<segment>
<wire x1="182.88" y1="83.82" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<label x="185.42" y="83.82" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="20.32" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<label x="25.4" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="USBDP" class="0">
<segment>
<wire x1="182.88" y1="81.28" x2="198.12" y2="81.28" width="0.1524" layer="91"/>
<label x="185.42" y="81.28" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="20.32" y1="55.88" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<label x="25.4" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="J11" gate="J1" pin="!RESET"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="91.44" y1="20.32" x2="91.44" y2="25.4" width="0.1524" layer="91"/>
<wire x1="91.44" y1="25.4" x2="104.14" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="91.44" y1="43.18" x2="91.44" y2="27.94" width="0.1524" layer="91"/>
<junction x="91.44" y="25.4"/>
<pinref part="S1" gate="G$1" pin="S1"/>
<wire x1="91.44" y1="27.94" x2="91.44" y2="25.4" width="0.1524" layer="91"/>
<wire x1="78.74" y1="25.4" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="27.94" x2="91.44" y2="27.94" width="0.1524" layer="91"/>
<junction x="91.44" y="27.94"/>
<wire x1="40.64" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<junction x="78.74" y="27.94"/>
<label x="43.18" y="27.94" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="RESETN"/>
</segment>
</net>
<net name="SWDCLK" class="0">
<segment>
<pinref part="J11" gate="J1" pin="SWDCLK/TCK"/>
<wire x1="40.64" y1="35.56" x2="60.96" y2="35.56" width="0.1524" layer="91"/>
<label x="43.18" y="35.56" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="60.96" y1="35.56" x2="60.96" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="182.88" y1="76.2" x2="198.12" y2="76.2" width="0.1524" layer="91"/>
<label x="185.42" y="76.2" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="J11" gate="J1" pin="SWDIO/TMS"/>
<wire x1="40.64" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<label x="43.18" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="182.88" y1="73.66" x2="198.12" y2="73.66" width="0.1524" layer="91"/>
<label x="185.42" y="73.66" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO"/>
</segment>
</net>
<net name="1-2-PA18" class="0">
<segment>
<wire x1="182.88" y1="99.06" x2="198.12" y2="99.06" width="0.1524" layer="91"/>
<label x="185.42" y="99.06" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="RO"/>
<wire x1="355.6" y1="40.64" x2="340.36" y2="40.64" width="0.1524" layer="91"/>
<label x="342.9" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="1-0-PA16" class="0">
<segment>
<wire x1="182.88" y1="104.14" x2="198.12" y2="104.14" width="0.1524" layer="91"/>
<label x="185.42" y="104.14" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0/TCC0-4"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="DI"/>
<wire x1="355.6" y1="71.12" x2="340.36" y2="71.12" width="0.1524" layer="91"/>
<label x="342.9" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="104.14" y1="12.7" x2="91.44" y2="12.7" width="0.1524" layer="91"/>
<wire x1="91.44" y1="12.7" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<junction x="91.44" y="12.7"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="78.74" y1="12.7" x2="78.74" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="P1"/>
<wire x1="78.74" y1="15.24" x2="78.74" y2="12.7" width="0.1524" layer="91"/>
<junction x="78.74" y="12.7"/>
<pinref part="U2" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="91.44" y1="91.44" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="78.74" y1="91.44" x2="78.74" y2="83.82" width="0.1524" layer="91"/>
<junction x="78.74" y="91.44"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="78.74" y1="137.16" x2="66.04" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="66.04" y1="137.16" x2="53.34" y2="137.16" width="0.1524" layer="91"/>
<junction x="66.04" y="137.16"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="53.34" y1="137.16" x2="40.64" y2="137.16" width="0.1524" layer="91"/>
<junction x="53.34" y="137.16"/>
<wire x1="40.64" y1="137.16" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<junction x="40.64" y="137.16"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="EPAD"/>
<wire x1="396.24" y1="15.24" x2="391.16" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="GND"/>
<wire x1="391.16" y1="12.7" x2="396.24" y2="12.7" width="0.1524" layer="91"/>
<wire x1="396.24" y1="12.7" x2="396.24" y2="15.24" width="0.1524" layer="91"/>
<wire x1="396.24" y1="12.7" x2="396.24" y2="7.62" width="0.1524" layer="91"/>
<junction x="396.24" y="12.7"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="396.24" y1="68.58" x2="411.48" y2="68.58" width="0.1524" layer="91"/>
<wire x1="396.24" y1="68.58" x2="396.24" y2="60.96" width="0.1524" layer="91"/>
<junction x="396.24" y="68.58"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="434.34" y1="71.12" x2="449.58" y2="71.12" width="0.1524" layer="91"/>
<wire x1="434.34" y1="71.12" x2="434.34" y2="63.5" width="0.1524" layer="91"/>
<junction x="434.34" y="71.12"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="325.12" y1="30.48" x2="327.66" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="320.04" y1="35.56" x2="337.82" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="38.1" y1="96.52" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="38.1" y1="99.06" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="20.32" y1="106.68" x2="20.32" y2="99.06" width="0.1524" layer="91"/>
<wire x1="20.32" y1="99.06" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
<junction x="38.1" y="99.06"/>
<wire x1="38.1" y1="99.06" x2="53.34" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="60.96" y1="106.68" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<wire x1="53.34" y1="99.06" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<junction x="53.34" y="106.68"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="312.42" y1="121.92" x2="317.5" y2="121.92" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="317.5" y1="121.92" x2="317.5" y2="119.38" width="0.1524" layer="91"/>
<wire x1="317.5" y1="119.38" x2="317.5" y2="116.84" width="0.1524" layer="91"/>
<junction x="317.5" y="119.38"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="317.5" y1="116.84" x2="312.42" y2="116.84" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="GND"/>
<wire x1="299.72" y1="119.38" x2="317.5" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="309.88" y1="144.78" x2="317.5" y2="144.78" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="317.5" y1="144.78" x2="317.5" y2="147.32" width="0.1524" layer="91"/>
<wire x1="317.5" y1="147.32" x2="317.5" y2="149.86" width="0.1524" layer="91"/>
<junction x="317.5" y="147.32"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="317.5" y1="149.86" x2="309.88" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="368.3" y1="134.62" x2="375.92" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="63.5" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J28" gate="G$1" pin="2"/>
<wire x1="294.64" y1="60.96" x2="274.32" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J37" gate="G$1" pin="2"/>
<wire x1="294.64" y1="35.56" x2="274.32" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-12.7" x2="274.32" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="294.64" y1="15.24" x2="276.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="276.86" y1="15.24" x2="276.86" y2="12.7" width="0.1524" layer="91"/>
<pinref part="J17" gate="G$1" pin="2"/>
<wire x1="276.86" y1="12.7" x2="294.64" y2="12.7" width="0.1524" layer="91"/>
<wire x1="276.86" y1="12.7" x2="274.32" y2="12.7" width="0.1524" layer="91"/>
<junction x="276.86" y="12.7"/>
<pinref part="J19" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J11" gate="J1" pin="GND@3"/>
<wire x1="10.16" y1="35.56" x2="2.54" y2="35.56" width="0.1524" layer="91"/>
<wire x1="2.54" y1="35.56" x2="2.54" y2="33.02" width="0.1524" layer="91"/>
<pinref part="J11" gate="J1" pin="GND@5"/>
<wire x1="2.54" y1="33.02" x2="10.16" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="2.54" y1="22.86" x2="2.54" y2="33.02" width="0.1524" layer="91"/>
<junction x="2.54" y="33.02"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="!RE"/>
<wire x1="355.6" y1="66.04" x2="337.82" y2="66.04" width="0.1524" layer="91"/>
<label x="342.9" y="66.04" size="1.778" layer="95"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="91.44" y1="53.34" x2="91.44" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="60.96" y1="53.34" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="104.14" y1="129.54" x2="99.06" y2="129.54" width="0.1524" layer="91"/>
<wire x1="99.06" y1="129.54" x2="99.06" y2="144.78" width="0.1524" layer="91"/>
<wire x1="99.06" y1="144.78" x2="104.14" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="78.74" y1="144.78" x2="99.06" y2="144.78" width="0.1524" layer="91"/>
<junction x="99.06" y="144.78"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="78.74" y1="144.78" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<junction x="78.74" y="144.78"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="66.04" y1="144.78" x2="53.34" y2="144.78" width="0.1524" layer="91"/>
<junction x="66.04" y="144.78"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="53.34" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<junction x="53.34" y="144.78"/>
<wire x1="40.64" y1="144.78" x2="40.64" y2="149.86" width="0.1524" layer="91"/>
<junction x="40.64" y="144.78"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="U2" gate="G$1" pin="VDDANA"/>
<pinref part="U2" gate="G$1" pin="VDDIO"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="VL"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="391.16" y1="76.2" x2="396.24" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="396.24" y1="76.2" x2="411.48" y2="76.2" width="0.1524" layer="91"/>
<junction x="396.24" y="76.2"/>
<wire x1="411.48" y1="76.2" x2="419.1" y2="76.2" width="0.1524" layer="91"/>
<junction x="411.48" y="76.2"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="474.98" y1="149.86" x2="472.44" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VOUT"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="50.8" y1="114.3" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="53.34" y1="114.3" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<junction x="53.34" y="114.3"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="60.96" y1="114.3" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<junction x="60.96" y="114.3"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="401.32" y1="149.86" x2="383.54" y2="149.86" width="0.1524" layer="91"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="396.24" y1="147.32" x2="401.32" y2="147.32" width="0.1524" layer="91"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="401.32" y1="134.62" x2="396.24" y2="134.62" width="0.1524" layer="91"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J13" gate="G$1" pin="2"/>
<wire x1="294.64" y1="5.08" x2="276.86" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<wire x1="0" y1="40.64" x2="0" y2="38.1" width="0.1524" layer="91"/>
<pinref part="J11" gate="J1" pin="VCC"/>
<wire x1="10.16" y1="38.1" x2="0" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VSW" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="99.06" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<label x="99.06" y="114.3" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="VSW"/>
</segment>
</net>
<net name="VDDCORE" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="104.14" y1="99.06" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="91.44" y1="99.06" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<junction x="91.44" y="99.06"/>
<wire x1="78.74" y1="99.06" x2="78.74" y2="114.3" width="0.1524" layer="91"/>
<junction x="78.74" y="99.06"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="78.74" y1="114.3" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<label x="91.44" y="99.06" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="VDDCORE"/>
</segment>
</net>
<net name="PA07" class="0">
<segment>
<wire x1="182.88" y1="127" x2="203.2" y2="127" width="0.1524" layer="91"/>
<label x="190.5" y="127" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA07/ADC0-7/SER0-3/TC1-1"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-2.54" x2="279.4" y2="-2.54" width="0.1524" layer="91"/>
<label x="281.94" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U3" gate="A" pin="VCC"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="391.16" y1="78.74" x2="434.34" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="434.34" y1="78.74" x2="449.58" y2="78.74" width="0.1524" layer="91"/>
<junction x="434.34" y="78.74"/>
<wire x1="449.58" y1="78.74" x2="457.2" y2="78.74" width="0.1524" layer="91"/>
<junction x="449.58" y="78.74"/>
<pinref part="P+7" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+9" gate="1" pin="+5V"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="10.16" y1="114.3" x2="20.32" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="20.32" y1="114.3" x2="25.4" y2="114.3" width="0.1524" layer="91"/>
<junction x="20.32" y="114.3"/>
<pinref part="U1" gate="G$1" pin="EN"/>
<wire x1="25.4" y1="114.3" x2="25.4" y2="109.22" width="0.1524" layer="91"/>
<junction x="25.4" y="114.3"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<pinref part="P+11" gate="1" pin="+5V"/>
<wire x1="20.32" y1="60.96" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="2"/>
<wire x1="294.64" y1="2.54" x2="276.86" y2="2.54" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="U3" gate="A" pin="A"/>
<wire x1="355.6" y1="55.88" x2="340.36" y2="55.88" width="0.1524" layer="91"/>
<label x="342.9" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J23" gate="G$1" pin="2"/>
<wire x1="294.64" y1="20.32" x2="279.4" y2="20.32" width="0.1524" layer="91"/>
<label x="281.94" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="U3" gate="A" pin="B"/>
<wire x1="355.6" y1="50.8" x2="340.36" y2="50.8" width="0.1524" layer="91"/>
<label x="342.9" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J21" gate="G$1" pin="2"/>
<wire x1="294.64" y1="17.78" x2="279.4" y2="17.78" width="0.1524" layer="91"/>
<label x="281.94" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="Z" class="0">
<segment>
<pinref part="U3" gate="A" pin="Z"/>
<wire x1="355.6" y1="20.32" x2="340.36" y2="20.32" width="0.1524" layer="91"/>
<label x="342.9" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J14" gate="G$1" pin="2"/>
<wire x1="294.64" y1="7.62" x2="279.4" y2="7.62" width="0.1524" layer="91"/>
<label x="281.94" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="Y" class="0">
<segment>
<pinref part="U3" gate="A" pin="Y"/>
<wire x1="355.6" y1="25.4" x2="340.36" y2="25.4" width="0.1524" layer="91"/>
<label x="342.9" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J15" gate="G$1" pin="2"/>
<wire x1="294.64" y1="10.16" x2="279.4" y2="10.16" width="0.1524" layer="91"/>
<label x="281.94" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="16"/>
<wire x1="454.66" y1="149.86" x2="457.2" y2="149.86" width="0.1524" layer="91"/>
<wire x1="457.2" y1="149.86" x2="457.2" y2="147.32" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="9"/>
<wire x1="457.2" y1="147.32" x2="457.2" y2="144.78" width="0.1524" layer="91"/>
<wire x1="457.2" y1="144.78" x2="457.2" y2="142.24" width="0.1524" layer="91"/>
<wire x1="457.2" y1="142.24" x2="457.2" y2="139.7" width="0.1524" layer="91"/>
<wire x1="457.2" y1="139.7" x2="457.2" y2="137.16" width="0.1524" layer="91"/>
<wire x1="457.2" y1="137.16" x2="457.2" y2="134.62" width="0.1524" layer="91"/>
<wire x1="457.2" y1="134.62" x2="457.2" y2="132.08" width="0.1524" layer="91"/>
<wire x1="457.2" y1="132.08" x2="454.66" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="10"/>
<wire x1="454.66" y1="134.62" x2="457.2" y2="134.62" width="0.1524" layer="91"/>
<junction x="457.2" y="134.62"/>
<pinref part="J10" gate="G$1" pin="11"/>
<wire x1="454.66" y1="137.16" x2="457.2" y2="137.16" width="0.1524" layer="91"/>
<junction x="457.2" y="137.16"/>
<pinref part="J10" gate="G$1" pin="12"/>
<wire x1="454.66" y1="139.7" x2="457.2" y2="139.7" width="0.1524" layer="91"/>
<junction x="457.2" y="139.7"/>
<pinref part="J10" gate="G$1" pin="13"/>
<wire x1="454.66" y1="142.24" x2="457.2" y2="142.24" width="0.1524" layer="91"/>
<junction x="457.2" y="142.24"/>
<pinref part="J10" gate="G$1" pin="14"/>
<wire x1="454.66" y1="144.78" x2="457.2" y2="144.78" width="0.1524" layer="91"/>
<junction x="457.2" y="144.78"/>
<pinref part="J10" gate="G$1" pin="15"/>
<wire x1="454.66" y1="147.32" x2="457.2" y2="147.32" width="0.1524" layer="91"/>
<junction x="457.2" y="147.32"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="457.2" y1="149.86" x2="462.28" y2="149.86" width="0.1524" layer="91"/>
<junction x="457.2" y="149.86"/>
</segment>
</net>
<net name="DIP1" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="429.26" y1="149.86" x2="421.64" y2="149.86" width="0.1524" layer="91"/>
<label x="421.64" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB05/ADC1-7"/>
<wire x1="182.88" y1="53.34" x2="198.12" y2="53.34" width="0.1524" layer="91"/>
<label x="185.42" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP2" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="429.26" y1="147.32" x2="421.64" y2="147.32" width="0.1524" layer="91"/>
<label x="421.64" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB04/ADC1-6"/>
<wire x1="182.88" y1="55.88" x2="198.12" y2="55.88" width="0.1524" layer="91"/>
<label x="185.42" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP3" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="3"/>
<wire x1="429.26" y1="144.78" x2="421.64" y2="144.78" width="0.1524" layer="91"/>
<label x="421.64" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB03/ADC0/SER5-1/TC6"/>
<wire x1="182.88" y1="58.42" x2="198.12" y2="58.42" width="0.1524" layer="91"/>
<label x="185.42" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP4" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="4"/>
<wire x1="429.26" y1="142.24" x2="421.64" y2="142.24" width="0.1524" layer="91"/>
<label x="421.64" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0"/>
<wire x1="182.88" y1="60.96" x2="198.12" y2="60.96" width="0.1524" layer="91"/>
<label x="185.42" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP5" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="5"/>
<wire x1="429.26" y1="139.7" x2="421.64" y2="139.7" width="0.1524" layer="91"/>
<label x="421.64" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB01/ADC0-13/SER5-3/TC7-1"/>
<wire x1="182.88" y1="63.5" x2="198.12" y2="63.5" width="0.1524" layer="91"/>
<label x="185.42" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP6" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="6"/>
<wire x1="429.26" y1="137.16" x2="421.64" y2="137.16" width="0.1524" layer="91"/>
<label x="421.64" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0"/>
<wire x1="182.88" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<label x="185.42" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP7" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="7"/>
<wire x1="429.26" y1="134.62" x2="421.64" y2="134.62" width="0.1524" layer="91"/>
<label x="421.64" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB31/SER7-1/SER5-0/TC0-1"/>
<wire x1="182.88" y1="12.7" x2="198.12" y2="12.7" width="0.1524" layer="91"/>
<label x="185.42" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP8" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="8"/>
<wire x1="429.26" y1="132.08" x2="421.64" y2="132.08" width="0.1524" layer="91"/>
<label x="421.64" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB30/SER7-0/SER5-1/TC0-0/SWO"/>
<wire x1="182.88" y1="15.24" x2="198.12" y2="15.24" width="0.1524" layer="91"/>
<label x="185.42" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="DE" class="0">
<segment>
<pinref part="U3" gate="A" pin="DE"/>
<wire x1="355.6" y1="45.72" x2="340.36" y2="45.72" width="0.1524" layer="91"/>
<label x="342.9" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="182.88" y1="96.52" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
<label x="185.42" y="96.52" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA19/SER1-3/SER3-3/TC3-1"/>
</segment>
</net>
<net name="TERM" class="0">
<segment>
<pinref part="U3" gate="A" pin="!TERM"/>
<wire x1="355.6" y1="60.96" x2="340.36" y2="60.96" width="0.1524" layer="91"/>
<label x="342.9" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="182.88" y1="101.6" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<label x="185.42" y="101.6" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA17/SER1-1/SER3-0/TC2-1/TCC0-5"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="U3" gate="A" pin="TERM100"/>
<wire x1="337.82" y1="30.48" x2="355.6" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U3" gate="A" pin="SRL"/>
<wire x1="347.98" y1="35.56" x2="355.6" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PA09" class="0">
<segment>
<wire x1="182.88" y1="121.92" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
<label x="190.5" y="121.92" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1"/>
</segment>
<segment>
<pinref part="J25" gate="G$1" pin="2"/>
<wire x1="294.64" y1="68.58" x2="279.4" y2="68.58" width="0.1524" layer="91"/>
<label x="281.94" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA08" class="0">
<segment>
<wire x1="182.88" y1="124.46" x2="203.2" y2="124.46" width="0.1524" layer="91"/>
<label x="190.5" y="124.46" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0"/>
</segment>
<segment>
<pinref part="J24" gate="G$1" pin="2"/>
<wire x1="294.64" y1="71.12" x2="279.4" y2="71.12" width="0.1524" layer="91"/>
<label x="281.94" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA10" class="0">
<segment>
<wire x1="182.88" y1="119.38" x2="203.2" y2="119.38" width="0.1524" layer="91"/>
<label x="190.5" y="119.38" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2"/>
</segment>
<segment>
<pinref part="J26" gate="G$1" pin="2"/>
<wire x1="294.64" y1="66.04" x2="279.4" y2="66.04" width="0.1524" layer="91"/>
<label x="281.94" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA11" class="0">
<segment>
<wire x1="182.88" y1="116.84" x2="203.2" y2="116.84" width="0.1524" layer="91"/>
<label x="190.5" y="116.84" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3"/>
</segment>
<segment>
<pinref part="J27" gate="G$1" pin="2"/>
<wire x1="294.64" y1="63.5" x2="279.4" y2="63.5" width="0.1524" layer="91"/>
<label x="281.94" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="XIN32" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="1"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="302.26" y1="149.86" x2="292.1" y2="149.86" width="0.1524" layer="91"/>
<wire x1="292.1" y1="149.86" x2="279.4" y2="149.86" width="0.1524" layer="91"/>
<junction x="292.1" y="149.86"/>
<label x="279.4" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0"/>
<wire x1="182.88" y1="144.78" x2="198.12" y2="144.78" width="0.1524" layer="91"/>
<label x="185.42" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="XIN0" class="0">
<segment>
<pinref part="Y2" gate="G$1" pin="1"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="304.8" y1="116.84" x2="292.1" y2="116.84" width="0.1524" layer="91"/>
<wire x1="292.1" y1="116.84" x2="279.4" y2="116.84" width="0.1524" layer="91"/>
<junction x="292.1" y="116.84"/>
<label x="279.4" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0"/>
<wire x1="182.88" y1="109.22" x2="198.12" y2="109.22" width="0.1524" layer="91"/>
<label x="185.42" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="386.08" y1="147.32" x2="370.84" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="373.38" y1="149.86" x2="358.14" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="386.08" y1="134.62" x2="383.54" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ERRLIGHT" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="350.52" y1="149.86" x2="335.28" y2="149.86" width="0.1524" layer="91"/>
<label x="337.82" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0"/>
<wire x1="182.88" y1="45.72" x2="198.12" y2="45.72" width="0.1524" layer="91"/>
<label x="185.42" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLKLIGHT" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="363.22" y1="147.32" x2="335.28" y2="147.32" width="0.1524" layer="91"/>
<label x="337.82" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PA27/GCLK-1"/>
<wire x1="182.88" y1="78.74" x2="198.12" y2="78.74" width="0.1524" layer="91"/>
<label x="185.42" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA23" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1"/>
<wire x1="182.88" y1="86.36" x2="203.2" y2="86.36" width="0.1524" layer="91"/>
<label x="190.5" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J41" gate="G$1" pin="2"/>
<wire x1="294.64" y1="25.4" x2="279.4" y2="25.4" width="0.1524" layer="91"/>
<label x="281.94" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA22" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PA22/SER3-0/SER5-1/TC4-0"/>
<wire x1="182.88" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<label x="190.5" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J40" gate="G$1" pin="2"/>
<wire x1="294.64" y1="27.94" x2="279.4" y2="27.94" width="0.1524" layer="91"/>
<label x="281.94" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA21" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PA21/SER5-3/SER3-3/TC7-1"/>
<wire x1="182.88" y1="91.44" x2="203.2" y2="91.44" width="0.1524" layer="91"/>
<label x="190.5" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J39" gate="G$1" pin="2"/>
<wire x1="294.64" y1="30.48" x2="279.4" y2="30.48" width="0.1524" layer="91"/>
<label x="281.94" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA20" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PA20/SER5-2/SER3-2/TC7-0"/>
<wire x1="182.88" y1="93.98" x2="203.2" y2="93.98" width="0.1524" layer="91"/>
<label x="190.5" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J38" gate="G$1" pin="2"/>
<wire x1="294.64" y1="33.02" x2="279.4" y2="33.02" width="0.1524" layer="91"/>
<label x="281.94" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB10" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB10/SER4-2/TC5-0/TCC0-4"/>
<wire x1="182.88" y1="40.64" x2="203.2" y2="40.64" width="0.1524" layer="91"/>
<label x="190.5" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J36" gate="G$1" pin="2"/>
<wire x1="294.64" y1="38.1" x2="279.4" y2="38.1" width="0.1524" layer="91"/>
<label x="281.94" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB11" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB11/SER4-3/TC5-1/TCC0-5"/>
<wire x1="182.88" y1="38.1" x2="203.2" y2="38.1" width="0.1524" layer="91"/>
<label x="190.5" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J35" gate="G$1" pin="2"/>
<wire x1="294.64" y1="40.64" x2="279.4" y2="40.64" width="0.1524" layer="91"/>
<label x="281.94" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB12" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB12/SER4-0/TC4-0"/>
<wire x1="182.88" y1="35.56" x2="203.2" y2="35.56" width="0.1524" layer="91"/>
<label x="190.5" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J34" gate="G$1" pin="2"/>
<wire x1="294.64" y1="43.18" x2="279.4" y2="43.18" width="0.1524" layer="91"/>
<label x="281.94" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB13" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB13/SER4-1/TC4-1"/>
<wire x1="182.88" y1="33.02" x2="203.2" y2="33.02" width="0.1524" layer="91"/>
<label x="190.5" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J33" gate="G$1" pin="2"/>
<wire x1="294.64" y1="45.72" x2="279.4" y2="45.72" width="0.1524" layer="91"/>
<label x="281.94" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB14" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB14/SER4-2/TC5-0"/>
<wire x1="182.88" y1="30.48" x2="203.2" y2="30.48" width="0.1524" layer="91"/>
<label x="190.5" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J29" gate="G$1" pin="2"/>
<wire x1="294.64" y1="58.42" x2="279.4" y2="58.42" width="0.1524" layer="91"/>
<label x="281.94" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB15" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB15/SER4-3/TC5-1"/>
<wire x1="182.88" y1="27.94" x2="203.2" y2="27.94" width="0.1524" layer="91"/>
<label x="190.5" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J30" gate="G$1" pin="2"/>
<wire x1="294.64" y1="55.88" x2="279.4" y2="55.88" width="0.1524" layer="91"/>
<label x="281.94" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA12" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PA12/SER2-0/SER4-1/TC2-0/TCC0-6"/>
<wire x1="182.88" y1="114.3" x2="203.2" y2="114.3" width="0.1524" layer="91"/>
<label x="190.5" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J31" gate="G$1" pin="2"/>
<wire x1="294.64" y1="53.34" x2="279.4" y2="53.34" width="0.1524" layer="91"/>
<label x="281.94" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA13" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PA13/SER2-1/SER4-0/TC2-1/TCC0-7"/>
<wire x1="182.88" y1="111.76" x2="203.2" y2="111.76" width="0.1524" layer="91"/>
<label x="190.5" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J32" gate="G$1" pin="2"/>
<wire x1="294.64" y1="50.8" x2="279.4" y2="50.8" width="0.1524" layer="91"/>
<label x="281.94" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB06" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB06/ADC1-8"/>
<wire x1="182.88" y1="50.8" x2="203.2" y2="50.8" width="0.1524" layer="91"/>
<label x="190.5" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-20.32" x2="279.4" y2="-20.32" width="0.1524" layer="91"/>
<label x="281.94" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB07" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB07/ADC1-9"/>
<wire x1="182.88" y1="48.26" x2="203.2" y2="48.26" width="0.1524" layer="91"/>
<label x="190.5" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-22.86" x2="279.4" y2="-22.86" width="0.1524" layer="91"/>
<label x="281.94" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="XOUT0" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="Y2" gate="G$1" pin="2"/>
<wire x1="292.1" y1="121.92" x2="304.8" y2="121.92" width="0.1524" layer="91"/>
<wire x1="292.1" y1="121.92" x2="279.4" y2="121.92" width="0.1524" layer="91"/>
<junction x="292.1" y="121.92"/>
<label x="279.4" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1"/>
<wire x1="182.88" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<label x="185.42" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="XOUT32" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="292.1" y1="144.78" x2="302.26" y2="144.78" width="0.1524" layer="91"/>
<wire x1="292.1" y1="144.78" x2="279.4" y2="144.78" width="0.1524" layer="91"/>
<junction x="292.1" y="144.78"/>
<label x="279.4" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1"/>
<wire x1="182.88" y1="142.24" x2="198.12" y2="142.24" width="0.1524" layer="91"/>
<label x="185.42" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA02" class="0">
<segment>
<wire x1="182.88" y1="139.7" x2="203.2" y2="139.7" width="0.1524" layer="91"/>
<label x="190.5" y="139.7" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA02/ADC0-1/DAC-0"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-15.24" x2="279.4" y2="-15.24" width="0.1524" layer="91"/>
<label x="281.94" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA03" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1"/>
<wire x1="182.88" y1="137.16" x2="203.2" y2="137.16" width="0.1524" layer="91"/>
<label x="190.5" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-17.78" x2="279.4" y2="-17.78" width="0.1524" layer="91"/>
<label x="281.94" y="-17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA04" class="0">
<segment>
<wire x1="182.88" y1="134.62" x2="203.2" y2="134.62" width="0.1524" layer="91"/>
<label x="190.5" y="134.62" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-10.16" x2="279.4" y2="-10.16" width="0.1524" layer="91"/>
<label x="281.94" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA05" class="0">
<segment>
<wire x1="182.88" y1="132.08" x2="203.2" y2="132.08" width="0.1524" layer="91"/>
<label x="190.5" y="132.08" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-7.62" x2="279.4" y2="-7.62" width="0.1524" layer="91"/>
<label x="281.94" y="-7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA06" class="0">
<segment>
<wire x1="182.88" y1="129.54" x2="203.2" y2="129.54" width="0.1524" layer="91"/>
<label x="190.5" y="129.54" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="294.64" y1="-5.08" x2="279.4" y2="-5.08" width="0.1524" layer="91"/>
<label x="281.94" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB16" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB16/SER5-0/TC6-0"/>
<wire x1="182.88" y1="25.4" x2="203.2" y2="25.4" width="0.1524" layer="91"/>
<label x="190.5" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J22" gate="G$1" pin="2"/>
<wire x1="294.64" y1="78.74" x2="279.4" y2="78.74" width="0.1524" layer="91"/>
<label x="281.94" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB17" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB17/SER5-1/TC6-1"/>
<wire x1="182.88" y1="22.86" x2="203.2" y2="22.86" width="0.1524" layer="91"/>
<label x="190.5" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J20" gate="G$1" pin="2"/>
<wire x1="294.64" y1="81.28" x2="279.4" y2="81.28" width="0.1524" layer="91"/>
<label x="281.94" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB22" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0"/>
<wire x1="182.88" y1="20.32" x2="203.2" y2="20.32" width="0.1524" layer="91"/>
<label x="190.5" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J18" gate="G$1" pin="2"/>
<wire x1="294.64" y1="83.82" x2="279.4" y2="83.82" width="0.1524" layer="91"/>
<label x="281.94" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB23" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1"/>
<wire x1="182.88" y1="17.78" x2="203.2" y2="17.78" width="0.1524" layer="91"/>
<label x="190.5" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J16" gate="G$1" pin="2"/>
<wire x1="294.64" y1="86.36" x2="279.4" y2="86.36" width="0.1524" layer="91"/>
<label x="281.94" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
